var scripts = [
    '/cookie.js',
    '/bitbucket.js'
];

function insertScript(data)
{
    var script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.innerHTML = data;
    document.head.appendChild(script);
}

scripts.forEach(function (script) {
    jQuery.get(chrome.extension.getURL(script), insertScript);
});








