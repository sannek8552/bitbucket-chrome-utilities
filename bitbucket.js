    /**
     * Stage list
     *
     * @type {Object}
     */
    var stageList = {
        'DONE'       : 'Готово',
        'TESTED'     : 'Протестировано',
        'IN PROGRESS': 'В процессе',
        'FAILED'     : 'Ошибка',
        'ON MASTER'  : 'Влито в мастер'
    };

    /**
     * Stage-class map
     *
     * @type {Object}
     */
    var stageClassMap = {
        'DONE'       : 'success',
        'TESTED'     : 'moved',
        'IN PROGRESS': 'current',
        'FAILED'     : 'error',
        'ON MASTER'  : 'complete'
    };

    /**
     * API Url
     *
     * @type {string}
     */
    var URL_ISSUE = 'https://bitbucket.org/api/1.0/repositories/{AUTHOR}/{REPO}/issues/';



    $.ajaxSetup({
        headers: {
            "X-CSRFToken": getCookie('csrftoken')
        }
    });

    var issueId = null, currentStage = '', author, repo,
        $titleContainer       = $('#issue-title'),
        $sideBarContainer     = $('.issue-attrs'),
        $issueHeaderContainer = $('#issue-header'),

        regexp = new RegExp(/^\/(\w+)\/(\w+)\/issue\/(\d+).*$/g),
        regexpIssueList = new RegExp(/^\/(\w+)\/(\w+)\/issues.*$/g),
        url = document.location.pathname;

    var matches = regexp.exec(url);
    if (matches) {
        /** Edit issue */
        author = matches[1];
        repo   = matches[2];
        URL_ISSUE = prepareUrl(author, repo);

        issueId = matches[3];

        //Find current stage
        var title = $titleContainer.text();
        var titleReg = new RegExp(/^\[([\w\s]*)\]/);
        var titleMatches = titleReg.exec(title);
        if (titleMatches && titleMatches[1]) {
            currentStage = titleMatches[1];

            var reg = new RegExp(/^\[[\w\s]*\]\s*/);
            title = title.replace(reg, '');
            $titleContainer.text(title);
        }

        var $btnContent = '<a href="#stage" id="stage" class="aui-button aui-style aui-dropdown2-trigger" style="margin-right: 10px;"' +
            'aria-owns="stage-dropdown" aria-controls="stage-dropdown">' +
            '<span class="dropdown-text">Стадия</span>' +
            '</a>';

        var $dropdownContent = $('<div id="stage-dropdown" class="aui-dropdown2 aui-style-default aui-dropdown2-in-buttons" ' +
            'data-dropdown2-alignment="left" aria-hidden="false">');
        var $linkContainer = $('<ul/>');
        $dropdownContent.html($linkContainer);

        $('.issue-toolbar')
            .prepend($dropdownContent)
            .prepend($btnContent);

        $dropdownContent.on('click', 'a:not(.disabled)', changeStageCallback);

        $('<dl class="stage"/>').appendTo($sideBarContainer);

        //delete status label and replace it with stage label
        $issueHeaderContainer.find('a:lt(1)').remove();
        $issueHeaderContainer.find('.issue-id').after('<span class="label"></span>');

        renderDropdown();
        renderSideBar();
        renderHeader()

    } else {
        /** Issue List */
        matches = regexpIssueList.exec(url);
        if (matches) {
            author = matches[1];
            repo   = matches[2];
            URL_ISSUE = prepareUrl(author, repo);

            $('.issues-list tr').each(function () {
                var $row = $(this), stageLabel = '', $td;
                if ($row.find('th').length) {
                    $td = $('<th style="width: 100px;"/>').append('<span class="aui-table-header-content">Стадия</span>');
                } else {
                    var reg = new RegExp(/^#\d+:\s*\[([\w\s]*)\].*$/),
                        $title = $row.find('td.text a');
                    var title = $title.text();
                    var matches = reg.exec(title);
                    if (matches) {
                        stageLabel = getStageLabel(matches[1]);
                        reg = new RegExp(/\[[\w\s]*\]\s*/);
                        title = title.replace(reg, '');
                        $title.text(title);
                    }
                    $td = $('<td class="stage"/>').append(stageLabel);

                }

                $row.prepend($td);
            });
        }
    }




    /*********************
     * * * FUNCTIONS * * *
     *********************/

    function prepareUrl(author, repo) {
        URL_ISSUE = URL_ISSUE.replace('{AUTHOR}', author);
        URL_ISSUE = URL_ISSUE.replace('{REPO}', repo);

        return URL_ISSUE;
    }
    /**
     * Stage change callback
     *
     */
    function changeStageCallback () {

        var stage = $(this).data('stage');
        if (!stage) {
            return;
        }

        var title = $titleContainer.text();

        var reg = new RegExp(/^\[[\w\s]*\]/);
        var stageString = "[" + stage + "]";
        if (reg.test(title)) {
            title = title.replace(reg, stageString);
        } else {
            title = stageString + ' ' + title;
        }

        var data = {
            title: title,
            stage: stage
        };

        updateIssue(issueId, data);
    }

    /**
     * Update issue title
     *
     * @param {Number} issueId
     * @param {Object} data
     */
    function updateIssue(issueId, data) {
        disableStageBtn();

        $.ajax({
            type: 'PUT',
            url: URL_ISSUE + issueId,
            data: {
                'title': data.title
            },
            success: function () {
                currentStage = data.stage;

                enableStageBtn();
                renderDropdown();
                renderSideBar();
                renderHeader();
            },
            error: function () {
                enableStageBtn();
            }
        });
    }

    /**
     * Render Stage select
     *
     */
    function renderDropdown () {
        $linkContainer.empty();
        _.each(stageList, function (title, value) {
            var cls = currentStage == value ? 'disabled' : '';
            $linkContainer.append('<li title="'+title+'"><a style="cursor: pointer;" data-stage="'+value+'" class="'+cls+'">'+getStageLabel(value)+'</a></li>');
        });
    }

    /**
     * Render SideBar
     *
     */
    function renderSideBar () {
        var $cont = $sideBarContainer.find('dl.stage'),
            stageLabel = getStageLabel();

        if (stageLabel) {
            $cont.html(
                '<dt>Стадия</dt>' +
                    '<dd>' +
                    stageLabel +
                    '</dd>'
            );
        }

    }

    /**
     * Render Header
     *
     */
    function renderHeader() {
        $issueHeaderContainer.find('.label').html(getStageLabel());
    }

    /**
     *  Get stage label
     *
     * @param {String} [stage]
     * @returns {string}
     */
    function getStageLabel(stage) {
        var disabled = stage && stage == currentStage ? 'disabled' : '';
        stage = stage || currentStage;

        if (!stage) {
            return '';
        }
        var cls = stageClassMap[stage] ? 'aui-lozenge-' + stageClassMap[stage] : '';

        return '<span class="'+disabled+' aui-lozenge '+cls+'">'+stage+'</span>';
    }

    /**
     * Enable stage selector
     *
     */
    function enableStageBtn() {
        $('#stage')
            .removeClass('loading')
            .attr({
                'aria-disabled': false,
                'disabled'     : false
            });
    }

    /**
     * Disable stage selector
     *
     */
    function disableStageBtn() {
        $('#stage')
            .addClass('loading')
            .attr({
                'aria-disabled': true,
                'disabled'     : true
            });
    }













